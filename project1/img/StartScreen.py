import pygame

class Start:

    def __init__(self):
        self.screen = pygame.display.get_surface()
        self.background_image = pygame.image.load("./img/background.png")
        self.start_image = pygame.image.load("./img/start.png")
        self.end_image = pygame.image.load("./img/end.png")
        self.start_position = (0,0)
        self.end_position = (0,0)
        self.done = False
        self.start = False


    def draw(self):
        self.screen.blit(self.background_image, (0, 0))
        self.screen.blit(self.start_image, self.start_position)
        self.screen.blit(self.start_image, self.end_position)

        pygame.display.flip()

    def button_click(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            x, y = pygame.mouse.get_pos()

            rect_start = self.start_image.get_rect()
            rect_end = self.end_image.get_rect()
            if rect_start.collidepoint(x,y):
                self.start = True
                self.done = True
            elif rect_end.collidepoint(x,y):
                self.done = True

    def event_loop(self):
        for event in pygame.event.get():
            self.button_click(event)
            self.draw()
            pygame.display.flip()

            if event.type == pygame.QUIT:
                self.done = True
                exit()

    def main_loop(self):
        self.draw()
        while not self.done:
            self.event_loop()

