import pygame


# resize images the window size
class Resize:

    def __init__(self):
        pass

    def resize(self, img):
        size = img.get_size()
        size = int(size[0] / 2), int(size[1] / 2)
        img = pygame.transform.scale(img, size)
        return img , size
