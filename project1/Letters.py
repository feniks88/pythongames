import pygame
import pygame
from project1.Resize import Resize
import random


# Letters class load images of letters and draw them on the background in random order
class Letters(Resize):

    def __init__(self):
        super().__init__()
        self.letters_img = {}
        self.position_w = 269
        self.position_h = 91
        self.picked_letters = []
        self.load_letters()
        self.letter_size = ()
        self.pick_letters()
        self.sorted_letters = self.picked_letters.copy()
        self.pars = []
        self.move = 0
        self.current_move = 0
        self.sort()

    # creates a dict, keys are the letters and values are the images
    def load_letters(self):
        letters = {'A', 'B', 'C', 'D', 'E', 'F','G', 'H', 'J', 'K', 'N', 'O', 'P', 'R', 'S', 'X', 'Z'}
        resized_letter = ()
        for key in letters:
            path = './img/letter_' + key + '.png'
            letter = pygame.image.load(path)
            resized_letter = self.resize(letter)
            self.letters_img.update({key:resized_letter[0]})
        self.letter_size = resized_letter[1]

    # draw a letter next to the latest position
    def draw_letter(self, key):
        self.screen.blit(self.letters_img[key], (self.position_w, self.position_h))
        self.position_w = self.position_w + 50

    # pick 10 random letters from letters_img
    def pick_letters(self):
       self.picked_letters = random.sample(self.letters_img.keys(), 10)

    # draw all picked random letters
    def draw_picked_letters(self):
        length = len(self.picked_letters)
        for i in range(length):
            self.draw_letter(self.picked_letters[i])
        self.position_w = 269
        self.position_h = 91

    # swap letters when you click on O button
    def swap_letters(self, frame_w):
        first_letter = self.find_letter(frame_w)
        if(self.picked_letters[first_letter] > self.picked_letters[first_letter + 1]):
            if(self.pars[self.current_move] == (self.picked_letters[first_letter], self.picked_letters[first_letter + 1])):
                tmp = self.picked_letters[first_letter]
                self.picked_letters[first_letter] = self.picked_letters[first_letter + 1]
                self.picked_letters[first_letter + 1] = tmp
                self.move -= 1
                self.current_move += 1
                return True
            else:
                return False
        else:
            return False

    # find the first letter in the frame
    def find_letter(self, width):
        letter = 0
        start = 269
        while(start != width):
            start += 50
            letter += 1
        return letter

    # BubbleSort for letters
    def sort(self):
        for passed in range(len(self.sorted_letters) - 1, 0, -1):
            for i in range(passed):
                if self.sorted_letters[i] > self.sorted_letters[i + 1]:
                    self.pars.append((self.sorted_letters[i], self.sorted_letters[i + 1]))
                    temp = self.sorted_letters[i]
                    self.sorted_letters[i] = self.sorted_letters[i + 1]
                    self.sorted_letters[i + 1] = temp
                    self.move += 1