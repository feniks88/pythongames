import pygame
from project1.Resize import Resize


# Button class loads images of buttons and draw them on the background
class Button(Resize):

    def __init__(self):
        super().__init__()
        self.screen = pygame.display.get_surface()
        self.buttons_img = {}
        self.buttons_size = {}
        self.load_buttons()
        self.buttons_light_img = {}
        self.buttons_light_size = {}
        self.load_buttons_light()
        self.buttons_position = {'L': (280, 165), 'O': (680, 165), 'R': (380, 165)}
        self.buttons_light_position = {'L': (270, 155), 'O': (670, 155), 'R': (370, 155)}
        self.buttons_on = {'L': 'off', 'O': 'off', 'R': 'off'}

    # draws 3 buttons on the background
    def draw_buttons(self):
        for key in self.buttons_img:
            if(self.buttons_on[key] == 'off'):
                self.screen.blit(self.buttons_img[key], self.buttons_position[key])
            else:
                self.screen.blit(self.buttons_light_img[key], self.buttons_light_position[key])

    # turns on highlight image of the button
    def button_up(self, key):
        self.buttons_on[key] = 'on'

    # turns off highlight image of the button
    def button_down(self, key):
        self.buttons_on[key] = 'off'

    # loads buttons, keys are the buttons names and values are the images
    def load_buttons(self):
        letters = {'L', 'O', 'R'}
        for key in letters:
            path = './img/button_' + key + '.png'
            button = pygame.image.load(path)
            resize_button = self.resize(button)
            self.buttons_img.update({key: resize_button[0]})
            self.buttons_size.update({key: resize_button[1]})

    # loads light buttons, keys are the buttons names and values are the images
    def load_buttons_light(self):
        letters = {'L', 'O', 'R'}
        for key in letters:
            path = './img/button_' + key + '_light.png'
            button = pygame.image.load(path)
            resize_button = self.resize(button)
            self.buttons_light_img.update({key: resize_button[0]})
            self.buttons_light_size.update({key: resize_button[1]})
