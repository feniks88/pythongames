import pygame

from project1.Resize import Resize


class StartScreen(Resize):

    def __init__(self,text):
        self.screen = pygame.display.get_surface()
        self.background_image = pygame.image.load("./img/background.png")
        self.start_image = pygame.image.load("./img/start.png")
        self.end_image = pygame.image.load("./img/end.png")
        self.start_position = (25, 350)
        self.end_position = (550, 355)
        self.start_size = (0, 0)
        self.end_size = (0, 0)
        self.done = False
        self.start = False
        self.input = text
        self.load_buttons()


    def draw(self):
        self.screen.blit(self.background_image, (0, 0))
        self.screen.blit(self.start_image, self.start_position)
        self.screen.blit(self.end_image, self.end_position)
        font = pygame.font.Font(None, 58)
        text = font.render(self.input, 1, (255, 255, 255))
        size = font.size(self.input)
        width = (self.screen.get_width()  - size[0]) / 2
        heigh = (self.screen.get_height() - size[1]) / 2 - 100
        textpos = (width, heigh)
        self.screen.blit(text, textpos)

        pygame.display.flip()
    def load_buttons(self):

        resized_start = self.resize((self.start_image))
        resized_end = self.resize((self.end_image))

        self.start_image = resized_start[0]
        self.end_image = resized_end[0]
        self.start_size = resized_start[1]
        self.end_size = resized_end[1]

    def button_click(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            x, y = pygame.mouse.get_pos()
            rect_start = pygame.Rect(self.start_position, self.start_size)
            rect_end = pygame.Rect(self.end_position, self.end_size)

            if rect_start.collidepoint(x,y):
                self.start = True
                self.done = True
            elif rect_end.collidepoint(x,y):
                self.done = True

    def event_loop(self):
        for event in pygame.event.get():
            self.button_click(event)
            self.draw()
            pygame.display.flip()

            if event.type == pygame.QUIT:
                self.done = True
                exit()

    def main_loop(self):
        self.draw()
        while not self.done:
            self.event_loop()

