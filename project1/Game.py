import pygame
from project1.Letters import Letters
from project1.Frame import Frame


class Game(Letters, Frame):

    def __init__(self, buttons):
        super().__init__()
        self.screen = pygame.display.get_surface()
        self.background_image = pygame.image.load("./img/chest.png")
        self.buttons = buttons
        self.done = False
        self.win = False

    # draw background picture
    def draw_background(self):
        screen_size = self.screen.get_size()
        self.background_image = pygame.transform.scale(self.background_image, screen_size)
        self.screen.blit(self.background_image, (0,0))

    # draw all the pictures
    def draw(self):
        self.draw_background()
        self.buttons.draw_buttons()
        self.draw_picked_letters()
        self.draw_frame()

        pygame.display.flip()

    # moves the frame when you click on the button
    def button_click(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            x, y = pygame.mouse.get_pos()
            for key in self.buttons.buttons_position:
                button_position = self.buttons.buttons_position[key]
                button_size = self.buttons.buttons_size[key]
                rect = pygame.Rect(button_position[0], button_position[1], button_size[0], button_size[1])
                if rect.collidepoint(x,y):
                    if key == 'R':
                        self.move_right()
                    if key == 'L':
                        self.move_left()
                    if key == 'O':
                        test = self.swap_letters(self.frame_w)
                        if test is False:
                            print(self.pars[self.current_move])
                            self.done = True
                        elif self.move == 0:
                            self.win = True
                            self.done = True


     # display highlighted button image
    def button_on(self, event):
        if event.type == pygame.MOUSEMOTION:
            x, y = pygame.mouse.get_pos()
            for key in self.buttons.buttons_position:
                button_position = self.buttons.buttons_position[key]
                button_size = self.buttons.buttons_size[key]
                rect = pygame.Rect(button_position[0], button_position[1], button_size[0], button_size[1])
                if rect.collidepoint(x, y):
                    self.buttons.button_up(key)
                else:
                    self.buttons.button_down(key)

    # main loop to load till game is closed
    def main_loop(self):
        self.draw()
        while not self.done:
            self.event_loop()

    # event loop to implements events
    def event_loop(self):
        for event in pygame.event.get():
            self.button_click(event)
            self.button_on(event)
            self.draw()
            pygame.display.flip()

            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

    # starts the game
    def run(self):
        self.main_loop()
