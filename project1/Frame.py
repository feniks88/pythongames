import pygame
from project1.Resize import Resize


# Frame class load image of the frame and draw it on the background
class Frame(Resize):

    def __init__(self):
        super().__init__()
        self.frame = pygame.image.load("./img/frame.png")
        resized_frame = self.resize(self.frame)
        self.frame = resized_frame[0]
        self.frame_size = resized_frame[1]
        self.frame_w = 269
        self.frame_h = 91
        self.rect_frame = pygame.Rect(self.frame_w, self.frame_h, self.frame_size[0] ,self.frame_size[1])

    # draw the frame at the current position
    def draw_frame(self):
        self.screen.blit(self.frame, (self.frame_w, self.frame_h))

    # move the frame position to the right
    def move_right(self):
        if self.frame_w < 669:
            self.frame_w += 50
        else:
            self.frame_w = 269
        self.frame_rect_update()

    # move the frame position to the left
    def move_left(self):
        if self.frame_w > 269:
            self.frame_w -= 50
        else:
            self.frame_w = 669
        self.frame_rect_update()

    # update the frame rect
    def frame_rect_update(self):
        self.rect_frame = pygame.Rect(self.frame_w, self.frame_h, self.frame_size[0] ,self.frame_size[1])