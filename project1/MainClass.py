
import pygame

from project1.Game import Game
from project1.Button import Button
from project1.StartScreen import StartScreen

pygame.init()
pygame.display.set_mode((960,540))
pygame.display.set_caption("Game")

buttons = Button()
game = Game(buttons)

open = StartScreen('Rozpocznij nowa gre')
while open.done is False or open.start:
    if game.done == False:
        open = StartScreen('Rozpocznij nowa gre')
    else:
        if game.win:
            open = StartScreen('Wygrana!')
        else:
            open = StartScreen('Przegrana!')
    open.main_loop()
    if open.start:
        game = Game(buttons)
        game.run()




pygame.quit()





